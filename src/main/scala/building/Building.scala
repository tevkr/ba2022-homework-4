package adt.tevkr.com.gitlab
package building

import scala.annotation.tailrec

object Genders extends Enumeration:
  val Male, Female = Value
case class Lodger (age: Int, gender: Genders.Value)

sealed trait Floor
case object Loft extends Floor
case class LivingFloor(firstLodger: Lodger, secondLodger: Lodger, nextFloor: Floor) extends Floor

case class Building(address: String, firstFloor: Floor)

def protoFold(building: Building, acc0: Int)(f: (Int, LivingFloor) => Int): Int =
  @tailrec
  def protoFoldRecursive(floor: Floor, acc: Int): Int = floor match
    case lf: LivingFloor => protoFoldRecursive(lf.nextFloor, f(acc, lf))
    case _ => acc
  protoFoldRecursive(building.firstFloor, acc0)

def countOldManFloors(building: Building, olderThen: Int): Int =
  def f(acc: Int, livingFloor: LivingFloor): Int = livingFloor match
    case LivingFloor(Lodger(age1, Genders.Male), Lodger(age2, Genders.Male), _)
      if age1 > olderThen || age2 > olderThen  => acc + 1
    case LivingFloor(Lodger(age, Genders.Male), _, _) if age > olderThen => acc + 1
    case LivingFloor(_, Lodger(age, Genders.Male), _) if age > olderThen => acc + 1
    case _ => acc
  protoFold(building, 0)(f)

def womanMaxAge(building: Building):Int =
  def f(acc: Int, livingFloor: LivingFloor): Int = livingFloor match
    case LivingFloor(Lodger(age1, Genders.Female), Lodger(age2, Genders.Female), _) =>
      acc max age1 max age2
    case LivingFloor(Lodger(age, Genders.Female), _, _) => acc max age
    case LivingFloor(_, Lodger(age, Genders.Female), _) => acc max age
    case _ => acc
  protoFold(building, 0)(f)