package adt.tevkr.com.gitlab
package building

object BuildingPlayGround extends App:
  val building = Building("some address",
    LivingFloor(Lodger(18, Genders.Male), Lodger(66, Genders.Male),
    LivingFloor(Lodger(19, Genders.Male), Lodger(20, Genders.Female),
    LivingFloor(Lodger(100, Genders.Female), Lodger(19, Genders.Male),
    LivingFloor(Lodger(34, Genders.Female), Lodger(25, Genders.Female),
    LivingFloor(Lodger(55, Genders.Male), Lodger(27, Genders.Male),
    Loft)))))
  )
  println(womanMaxAge(building))// 100
  println(countOldManFloors(building, 54))// 2
