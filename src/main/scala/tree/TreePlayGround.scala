package adt.tevkr.com.gitlab
package tree

object TreePlayGround extends App:
  val tree0 = Node(1, Node(2, RedLeaf, RedLeaf), Node(4, GreenLeaf, YellowLeaf))
  val tree1 = GreenLeaf
  val tree2 = Node(1, RedLeaf, GreenLeaf)
  val tree3 = Node(4, Node(6, RedLeaf, RedLeaf), RedLeaf)
  println(countYellowAndRedValues(tree0))// 6
  println(countYellowAndRedValues(tree1))// 0
  println(countYellowAndRedValues(tree2))// 1
  println(countYellowAndRedValues(tree3))// 10

  println(maxValue(tree0))// Some(4)
  println(maxValue(tree1))// None
  println(maxValue(tree2))// Some(1)
  println(maxValue(tree3))// Some(6)
