package adt.tevkr.com.gitlab
package tree

sealed trait Tree

case class Node(value: Int, left: Tree, right: Tree) extends Tree

case object RedLeaf extends Tree
case object YellowLeaf extends Tree
case object GreenLeaf extends Tree

def countYellowAndRedValues(tree: Tree): Int = tree match
  case Node(value, left, RedLeaf | YellowLeaf) => value + countYellowAndRedValues(left)
  case Node(value, RedLeaf | YellowLeaf, right) => value + countYellowAndRedValues(right)
  case Node(_, left, right) => countYellowAndRedValues(left) + countYellowAndRedValues(right)
  case _ => 0

def maxValue(tree: Tree): Option[Int] = tree match
  case n: Node => List(Some(n.value), maxValue(n.left), maxValue(n.right)).flatten match
    case Nil => None
    case xs => Some(xs.max)
  case _ => None