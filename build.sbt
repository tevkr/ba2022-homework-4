ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "3.2.0"

lazy val root = (project in file("."))
  .settings(
    name := "ba2022-homework-4",
    idePackagePrefix := Some("adt.tevkr.com.gitlab")
  )
